package test;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;

import beans.Student;

public class StudTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Session ses = new AnnotationConfiguration().configure().buildSessionFactory().getCurrentSession();
		Transaction t = ses.beginTransaction();
		System.out.println("first level cache demo - getting same recored under same session");
		
		Student a = (Student)ses.get(Student.class, 1);
		System.out.println(a.getSname());
		
		Student b = (Student)ses.get(Student.class, 1);
		System.out.println(b.getSname());
		
//		t.commit();
//		ses.close();	
		System.out.println("Second level cache demo - getting record from session factory cache");
		Session ses1 = new AnnotationConfiguration().configure().buildSessionFactory().getCurrentSession();
		Transaction t1 = ses1.beginTransaction();
		Student c = (Student)ses1.get(Student.class, 1);
		System.out.println(c.getSname());
		Student d = (Student)ses1.get(Student.class, 1);

		ses1.beginTransaction();
		System.out.println(d.getSname());
		
	}
}
